<?php
require_once "views/ViewCart.php";
require_once "views/ViewProduct.php";
require_once "views/ViewCheckout.php";

/**
 * Fichier définissant le controlleur cart
 */

/**
 * Class ControllerCart est la classe permettant de gérer le panier
 *
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ControllerCart
{
    /**
     * @var Model Le modèle à utiliser pour récupérer les données de la BDD
     */
    private $connDB;
    /**
     * @var ViewCart La vue du panier
     */
    private $viewCart;
    /**
     * @var ViewProduct La vue d'un produit
     */
    private $viewProduct;
    /**
     * @var ViewCheckout La vue de confirmation de commande
     */
    private $viewCheckout;

    /**
     * ControllerCart constructor.
     * @param Model $db la connexion à la base de données
     */
    public function __construct(Model $db)
    {
        $this->connDB = $db;
        $this->viewCart = new ViewCart();
        $this->viewProduct = new ViewProduct();
        $this->viewCheckout = new ViewCheckout();
    }

    /**
     * Méthode utilisée pour afficher le panier de l'utilisateur
     */
    public function renderCart()
    {
        $books = array();
        foreach (array_keys($_SESSION['cart']) as $idProduct) {
            $books[$idProduct] = array(
                'book' => $this->connDB->getProductDetails($idProduct)['productInfo'],
                'quantity' => $_SESSION['cart'][$idProduct],
            );
        }
        $this->viewCart->render($books);
    }

    /**
     * Méthode utilisée pour ajouter un produit
     * @param $idProduct string l'id du produit à ajouter
     * @param $quantity string la quantité à ajouter
     */
    public function addProduct($idProduct, $quantity)
    {
        if (isset($_SESSION['cart'][$idProduct])) {
            $_SESSION['cart'][$idProduct] += $quantity;
        } else {
            $_SESSION['cart'][$idProduct] = (int)$quantity;
        }
        if ($quantity <= 0) {
            if ($_SESSION['cart'][$idProduct] <= 0) {
                unset($_SESSION['cart'][$idProduct]);
            }
            header('Location: index.php?viewProduct=' . $idProduct . '&removed');
        } else {
            header('Location: index.php?viewProduct=' . $idProduct . '&added');
        }
    }

    /**
     * Méthode utilisée pour afficher la vue de confirmation de commande
     */
    public function checkout()
    {
        $books = array();
        foreach (array_keys($_SESSION['cart']) as $idProduct) {
            $books[0][$idProduct] = array(
                'book' => $this->connDB->getProductDetails($idProduct)['productInfo'],
                'quantity' => $_SESSION['cart'][$idProduct],
            );
        }
        $books[1] = $this->connDB->getUserInfo($_SESSION['user']);
        $this->viewCheckout->render($books);
    }

    /**
     * Méthode utilisée pour payer
     * @param $postData string Les données d'adresse et de méthode de paiement (NON UTILISE)
     * @param $cart string Le panier de l'utilisateur
     * @param $userID string l'ID de l'utilisateur
     */
    public function pay($postData, $cart, $userID)
    {
        //Postdata n'est pas utilisée car nous ne stockons pas d'adresse
        // ni de méthode de paiement dans les commandes
        // CHOIX D'IMPLEMENTATION
        $this->connDB->createCommand($userID, $cart);
        $_SESSION['cart'] = array();
        header('Location: index.php');
    }

}