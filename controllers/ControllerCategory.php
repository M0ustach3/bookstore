<?php
require_once "views/ViewCategory.php";
require_once "views/components/Filter.php";

/**
 * Classe définissant le controleur category
 */

/**
 * Class ControllerCategory représente le contrôleur gérant les catégories
 *
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ControllerCategory
{
    /**
     * @var Model Le modèle à utiliser pour récupérer les données de la BDD
     */
    private $connDB;
    /**
     * @var ViewCategory La vue d'une catégorie
     */
    private $viewCategory;

    /**
     * ControllerCategory constructor.
     * @param Model $connDB le modèle à utiliser
     */
    public function __construct(Model $connDB)
    {
        $this->connDB = $connDB;
        $this->viewCategory = new ViewCategory();
    }

    /**
     * Méthode utilisée pour afficher les 100 premières catégories de la base de données
     */
    public function renderCategories()
    {
        $this->viewCategory->render(array('categories' => $this->connDB->getCategories(100)));
    }

    /**
     * Méthode utilisée pour afficher les produits d'une catégorie
     * @param $categoryId string l'id de la catégorie à afficher
     */
    public function renderCategory($categoryId)
    {
        $products = $this->connDB->getProductsByCategoryID($categoryId);
        if (empty($products)) {
            $this->viewCategory->render('Aucun produit disponible pour cette catégorie');
        } else {
            $this->viewCategory->render(array('books' => $products));
        }
    }
}