<?php
require_once "views/ViewFiltering.php";
/**
 * Class ControllerFiltering représente le contrôleur pour les filtres
 */

/**
 * Class ControllerFiltering représente le contrôleur pour les filtres
 *
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ControllerFiltering
{
    /**
     * @var Model Le modèle à utiliser pour récupérer les données de la BDD
     */
    private $connDB;
    /**
     * @var ViewFiltering La vue d'affichage des résultats filtrés
     */
    private $viewFiltering;

    /**
     * ControllerFiltering constructor.
     * @param Model $connDB Le modèle à utiliser
     */
    public function __construct(Model $connDB)
    {
        $this->connDB = $connDB;
        $this->viewFiltering = new ViewFiltering();
    }

    /**
     * Méthode utilisée pour afficher la page de résultats filtrés
     * @param $get array les paramètres url
     */
    public function renderFilterPage($get)
    {
        if (isset($get['search'])) {
            $filteredBooks = $this->connDB->getBooksBySearchTerm($get['search']);
            $this->viewFiltering->render($filteredBooks);
        } else {
            $filteredBooks = $this->connDB->filterBooks($get);
            $this->viewFiltering->render($filteredBooks);
        }
    }
}