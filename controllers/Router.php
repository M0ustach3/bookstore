<?php
require_once "models/Model.php";
require_once "controllers/ControllerHome.php";
require_once "controllers/ControllerLogin.php";
require_once "controllers/ControllerAccount.php";
require_once "controllers/ControllerProduct.php";
require_once "controllers/ControllerCategory.php";
require_once "controllers/ControllerFiltering.php";
require_once "controllers/ControllerCart.php";

/**
 * Classe définissant le routeur
 */

/**
 * Class Router représente une classe répartissant la demande de l'utilisateur vers le contrôleur adapté
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class Router
{
    /**
     * @var Model Le modèle à utiliser pour récupérer les données de la BDD
     */
    private $connDB;
    /**
     * @var ControllerHome Le contrôleur de page d'accueil
     */
    private $controllerHome;
    /**
     * @var ControllerLogin Le contrôleur de connexion et d'inscription
     */
    private $controllerLogin;
    /**
     * @var ControllerAccount Le contrôleur de compte d'utilisateur
     */
    private $controllerAccount;
    /**
     * @var ControllerProduct Le contrôleur de page produit
     */
    private $controllerProduct;
    /**
     * @var ControllerCategory Le contrôleur de catégorie
     */
    private $controllerCategory;
    /**
     * @var ControllerFiltering Le contrôleur de filtres
     */
    private $controllerFiltering;
    /**
     * @var ControllerCart Le contrôleur de panier
     */
    private $controllerCart;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->connDB = new Model();
        $this->controllerHome = new ControllerHome($this->connDB);
        $this->controllerLogin = new ControllerLogin($this->connDB);
        $this->controllerAccount = new ControllerAccount($this->connDB);
        $this->controllerProduct = new ControllerProduct($this->connDB);
        $this->controllerCategory = new ControllerCategory($this->connDB);
        $this->controllerFiltering = new ControllerFiltering($this->connDB);
        $this->controllerCart = new ControllerCart($this->connDB);
    }

    /**
     * Méthode permettant de rediriger l'utilisateur sur la page nécessaire
     */
    public function routeRequest()
    {
        //The user disconnects
        if (isset($_GET['disconnect'])) {
            session_unset();
            session_destroy();
            header('Location: index.php');
            return;
        }

        // Set the cart session var
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = array();
        }

        //A request is made to create a user
        if ((isset($_GET['connect']) && isset($_GET['new'])) && (isset($_POST['user'])
                && isset($_POST['password']))) {
            $this->controllerLogin->createAccount($_POST['surname'], $_POST['name'], $_POST['address'], $_POST['zipcode'], $_POST['city'], $_POST['birth'], $_POST['user'], $_POST['password']);
            return;
        }
        //A request is made to login
        if (isset($_GET['connect']) && (isset($_POST['user']) && isset($_POST['password']))) {
            $this->controllerLogin->login($_POST['user'], $_POST['password']);
            return;
        }
        // Click on the connect button and the user is not logged in
        if (isset($_GET['connect']) && !isset($_SESSION['user'])) {
            //Create an account
            if (isset($_GET['new'])) {
                $this->controllerLogin->printCreateAccountPage();
                return;
            } else {
                $this->controllerLogin->printLoginPage();
                return;
            }
        }

        if (isset($_GET['viewProduct']) && !empty($_GET['viewProduct'])) {
            $this->controllerProduct->renderProduct($_GET['viewProduct']);
            return;
        }

        // The user wants a category
        if (isset($_GET['category'])) {
            // All the categories
            if (empty($_GET['category'])) {
                $this->controllerCategory->renderCategories();
                return;
                //A specific category
            } else {
                $this->controllerCategory->renderCategory($_GET['category']);
                return;
            }
        }

        //The user wants to filter
        if (isset($_GET['advancedSearch']) || isset($_GET['search'])) {
            $this->controllerFiltering->renderFilterPage($_GET);
            return;
        }

        //The user wants to see his cart
        if (isset($_GET['cart'])) {
            $this->controllerCart->renderCart();
            return;
        }
        if (isset($_GET['addProduct']) && isset($_GET['add'])) {
            $this->controllerCart->addProduct($_GET['addProduct'], $_GET['add']);
            return;
        }

        //Wants to checkout without being logged in
        if (!isset($_SESSION['user']) && isset($_GET['checkout'])) {
            $this->controllerLogin->printLoginPage();
            return;
        }

        //Wants to checkout and is logged in
        if (isset($_SESSION['user']) && isset($_GET['checkout'])) {
            $this->controllerCart->checkout();
            return;
        }

        //Wants to rate a product
        if (isset($_SESSION['user']) && isset($_GET['rating'])
            && isset($_POST['ratingValue']) && isset($_POST['commentValue']) && isset($_POST['bookID'])
        ) {
            $this->controllerAccount->rateProduct($_POST['bookID'], $_POST['ratingValue'], $_POST['commentValue'], $_SESSION['user']);
            return;
        }

        // The user is logged in
        if (isset($_SESSION['user'])) {
            if (isset($_GET['account'])) {
                $this->controllerAccount->renderProfile($_SESSION['user']);
                return;
            } else if (isset($_GET['confirmCheckout'])) {
                $this->controllerCart->pay($_POST, $_SESSION['cart'], $_SESSION['userID']);
                return;
            }
        }

        $this->controllerHome->renderHome();
    }
}