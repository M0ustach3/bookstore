<?php
require_once "models/Model.php";
require_once "views/ViewProduct.php";

/**
 * Class ControllerProduct représente la gestion d'affichage d'un produit
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ControllerProduct
{
    /**
     * @var ViewProduct La vue d'un produit
     */
    private $viewProduct;
    /**
     * @var Model Le modèle à utiliser pour récupérer les données de la BDD
     */
    private $connDB;

    /**
     * ControllerProduct constructor.
     * @param Model $db le modèle à utiliser
     */
    public function __construct(Model $db)
    {
        $this->connDB = $db;
        $this->viewProduct = new ViewProduct();
    }

    /**
     * Méthode utilisée pour afficher la page de détails d'un produit
     * @param $bookID string l'id d'un produit
     */
    public function renderProduct($bookID)
    {
        $productDetails = $this->connDB->getProductDetails($bookID);
        if ($productDetails['productInfo'] === false) {
            $this->viewProduct->render('Pas de données pour ce produit');
        } else {
            $this->viewProduct->render($productDetails);
        }
    }
}