<?php
require_once "views/createAndLogin/ViewCreateUser.php";
require_once "views/createAndLogin/ViewLogin.php";

/**
 * Class ControllerLogin représente la gestion de la connexion et de l'inscription d'un utilisateur
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ControllerLogin
{
    /**
     * @var Model Le modèle à utiliser pour récupérer les données de la BDD
     */
    private $conndb;
    /**
     * @var ViewCreateUser La vue de création d'utilisateur
     */
    private $viewCreateUser;
    /**
     * @var ViewLogin La vue de connexion
     */
    private $viewLogin;

    /**
     * ControllerLogin constructor.
     * @param Model $conn le modèle à utiliser
     */
    public function __construct(Model $conn)
    {
        $this->conndb = $conn;
        $this->viewCreateUser = new ViewCreateUser();
        $this->viewLogin = new ViewLogin();
    }

    /**
     * Méthode utilisée pour créer un compte
     * @param $surname string le nom de famille
     * @param $name string le prénom
     * @param $address string l'adresse
     * @param $zipcode string le code postal
     * @param $city string la ville
     * @param $birth string la date de naissance
     * @param $user string le nom d'utilisateur
     * @param $password string le mot de passe
     */
    public function createAccount($surname, $name, $address, $zipcode, $city, $birth, $user, $password)
    {
        $result = $this->conndb->createUser($surname, $name, $address, $zipcode, $city, $birth, $user, $password);
        if ($result === "success") {
            $this->viewLogin->render(array("success" => 'Vous pouvez maintenant vous connecter !'));
        } else if ($result === "userExists") {
            $this->viewCreateUser->render(array(
                "error" => 'Un autre utilisateur possède déjà ce pseudo :(',
                "values" => array(
                    "surname" => $surname,
                    "name" => $name,
                    "address" => $address,
                    "zipcode" => $zipcode,
                    "city" => $city,
                    "birth" => $birth,
                    "user" => $user,
                    "password" => $password
                )
            ));
        } else if ($result === "noPasswdOrUser") {
            $this->viewCreateUser->render(array(
                "error" => 'Un pseudo ET un mot de passe sont requis',
                "values" => array(
                    "surname" => $surname,
                    "name" => $name,
                    "address" => $address,
                    "zipcode" => $zipcode,
                    "city" => $city,
                    "birth" => $birth,
                    "user" => $user,
                    "password" => $password
                )
            ));
        }
    }

    /**
     * Méthode utilisée pour la connexion de l'utilisateur
     * @param $user string le nom d'utilisateur
     * @param $password string le mot de passe de l'utilisateur
     */
    public function login($user, $password)
    {
        if ($this->conndb->isUserValid($user, $password)) {
            $userData = $this->conndb->getUserInfo($user);
            $_SESSION['user'] = htmlspecialchars($userData['username']);
            $_SESSION['userID'] = htmlspecialchars($userData['id']);
            header('Location: index.php');
        } else {
            $this->viewLogin->render(array("error" => 'Mauvaise combinaison utilisateur/mot de passe'));
        }
    }

    /**
     * Méthode utilisée pour afficher la page de login
     */
    public function printLoginPage()
    {
        $this->viewLogin->render(array());
    }

    /**
     * Méthode utilisée pour afficher la page de création de compte
     */
    public function printCreateAccountPage()
    {
        $this->viewCreateUser->render('');
    }
}