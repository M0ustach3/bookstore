<?php
require_once 'views/ViewProfile.php';


/**
 * Fichier définissant le controleur account
 */

/**
 * Class ControllerAccount est le contrôleur gérant le compte d'utilisateur
 *
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ControllerAccount
{
    /**
     * @var Model Le modèle à utiliser pour récupérer les données de la BDD
     */
    private $connDB;
    /**
     * @var ViewProfile La vue du profil à générer
     */
    private $viewProfile;

    /**
     * ControllerAccount constructor.
     * @param Model $db le modèle à utiliser
     */
    public function __construct(Model $db)
    {
        $this->connDB = $db;
        $this->viewProfile = new ViewProfile();
    }

    /**
     * Méthode utilisée pour afficher le profil de l'utilisateur connecté
     * @param $user string le nom de l'utilisateur
     */
    public function renderProfile($user)
    {
        $this->viewProfile->render(array(
            "profile" => $this->connDB->getUserInfo($user),
            "commands" => $this->connDB->getUserCommands($user),
            "ratings" => $this->connDB->getNonRatedProducts($user)
        ));
    }

    /** Méthode utilisée pour noter un produit
     * @param $bookID string le numéro du livre à noter
     * @param $rating string la valeur de la note du livre
     * @param $comment string le commentaire du livre
     * @param $user string le nom d'utilisateur
     */
    public function rateProduct($bookID, $rating, $comment, $user)
    {
        $isInserted = $this->connDB->rateProduct($bookID, $rating, $comment, $user);
        if (!$isInserted) {
            $this->viewProfile->render(array(
                "profile" => $this->connDB->getUserInfo($user),
                "commands" => $this->connDB->getUserCommands($user),
                "ratings" => $this->connDB->getNonRatedProducts($user),
                "error" => "Le commentaire et la note ne doivent pas être vide"
            ));
        } else {
            $this->viewProfile->render(array(
                "profile" => $this->connDB->getUserInfo($user),
                "commands" => $this->connDB->getUserCommands($user),
                "ratings" => $this->connDB->getNonRatedProducts($user),
                "success" => "Le produit à bien été noté"
            ));
        }
    }
}