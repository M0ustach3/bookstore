<?php
require_once "models/Model.php";
require_once "views/ViewHome.php";

/**
 * Class ControllerHome représente le contrôleur de gestion de la page d'accueil
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ControllerHome
{
    /**
     * @var Model Le modèle à utiliser pour récupérer les données de la BDD
     */
    private $connDb;
    /**
     * @var ViewHome La vue de la page d'accueil
     */
    private $viewHome;

    /**
     * ControllerHome constructor.
     * @param Model $connDb le modèle à utiliser
     */
    public function __construct(Model $connDb)
    {
        $this->connDb = $connDb;
        $this->viewHome = new ViewHome();
    }

    /**
     * Méthode utilisée pour afficher la page d'accueil, avec 4 livres aléatoires et 4 catégories
     */
    public function renderHome()
    {
        $data = array($this->connDb->getRandomBooks(4), $this->connDb->getCategories(4));
        $this->viewHome->render($data);
    }

}