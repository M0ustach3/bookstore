<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--THESE ARE JUST ICONS, WE DIDN'T WANT TO CREATE ICONS-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <!-- THIS IS THE MONTSERRAT FONT-->
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="manifest" href="includes/manifest.json">
    <meta name="theme-color" content="#2196f3">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <title>Bookstore |
        <?php
        if ((isset($_GET['connect']) && isset($_GET['new']))) {
            echo "Inscription";
        } else if ((isset($_GET['connect']))) {
            echo "Connexion";
        } else if (isset($_GET['viewProduct'])) {
            echo "Produit";
        } else if (isset($_GET['category'])) {
            echo "Catégorie";
        } else if (isset($_GET['advancedSearch']) || isset($_GET['search'])) {
            echo "Recherche";
        } else if (isset($_GET['cart'])) {
            echo "Panier";
        } else if (isset($_GET['checkout'])) {
            echo "Paiement";
        } else {
            echo "Accueil";
        }
        ?>


    </title>
</head>
<header>
    <div>
        <ul class="headerRow">
            <li class="headerItem">
                <ul>
                    <li class="headerItem"><a id="websiteName" href="index.php">Bookstore</a>
                    </li>
                    <li class="headerItem">
                        <div id="changeThemeDiv">
                            <div><i class="material-icons">brightness_5</i></div>
                            <div><label class="switch">
                                    <input type="checkbox" id="change">
                                    <span class="slider round"></span>
                                </label></div>
                            <div><i class="material-icons">
                                    brightness_2
                                </i></div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="headerItem">
                <div class="search-container">
                    <form action="index.php" method="GET">
                        <input type="text" placeholder="Rechercher..." name="search" autocomplete="off">
                        <button type="submit"><i class="material-icons">search</i></button>
                    </form>
                </div>
            </li>
            <?php
            $compte = 0;
            if (isset($_SESSION['cart'])) {
                foreach ($_SESSION['cart'] as $item) {
                    $compte += $item;
                }
            }
            if (isset($_SESSION['user'])) {
                ?>
                <ul>
                    <li class="headerItem"><a href="index.php?cart"><i class="material-icons">
                                shopping_cart
                            </i></a></li>
                    <li class="headerItem"><a href="index.php?account"><i class="material-icons">
                                account_circle
                            </i></a></li>
                    <li class="headerItem"><a href="index.php?disconnect"><i class="material-icons">
                                exit_to_app
                            </i></a></li>
                </ul>
                <?php
            } else {
                ?>
                <ul>
                    <li class="headerItem"><a href="index.php?cart"><i class="material-icons">
                                shopping_cart
                            </i></a></li>
                    <li class="headerItem"><a href="index.php?connect"><i class="material-icons">
                                account_circle
                            </i></a></li>
                </ul>
                <?php
            }
            ?>
        </ul>
    </div>
    <div>
        <ul class="headerRow">
            <?php
            $m = new Model();

            $categories = $m->getTopCategories(3);

            foreach ($categories as $category) {
                ?>
                <li class="headerItem">
                    <a href="index.php?category=<?= $category['id'] ?>"><?= $category['name'] ?></a>
                </li>
                <?php
            }

            $m->__destruct();
            ?>
            <li class="headerItem"><a href="index.php?category">Plus de genres...</a></li>
        </ul>
    </div>
</header>
<body>