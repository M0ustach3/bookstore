# BookStore

Projet de fin de module de Programmation WEB portant sur un site d'e-commerce de livres.

Réalisé par Pablo BONDIA-LUTTIAU et Lennaïg CHARRON

Vous pouvez retrouver une version interprétée de ce README ([https://gitlab.com/M0ustach3/bookstore/blob/master/README.md](https://gitlab.com/M0ustach3/bookstore/blob/master/README.md)) afin
d'apprécier la mise en forme que nous avons effectué pour ce document.

Nous avons également réalisé de la documentation pour toutes les classes, méthodes et attributs de notre projet.
Vous pouvez y accéder en se rendant dans le dossier ``docs/`` et en double-cliquant sur le fichier ``index.html``.
Appréciez la belle documentation !

## Installation
## TRES IMPORTANT

Vous **DEVEZ** changer les identifiants définis dans la classe ``config/Config.php``. Remplissez les avec
vos propres variables correpondant à votre configuration de base de données. La base de données à été 
exportée avec le nom ``bondialuttiaucharron`` dans le fichier ``config/BONDIALUTTIAUCHARRON.sql``, veillez à ne
**PAS** changer la constante de nom de la base de données.

De plus, le driver PDO à besoin d'être installé pour que le projet fonctionne. Nous avons testé avec plusieurs
configuration (une avec MAMP sous MacOS, une avec WAMP sous Windows, et une directement avec la commande 
``php -S localhost:8080``) et dans tous les cas, le driver était installé par défaut, tout devrait ainsi fonctionner
sans aucune configuration supplémentaire. __CEPENDANT__ nous avons créé un VirtualHost WAMP, nous ne savons pas si cela peut 
empêcher le site de fonctionner, mais nous le précisons au cas ou. 

## Fonctionnalités
### Nécessaires
- Design du site réalisé entièrement avec Flexbox
- Génération dynamique de pages via PHP
- Gestion des produits, clients et commandes via une base de données MySQL/MariaDB
- Animations CSS
### Bonus réalisés
- Version mobile du site, totalement responsive pour les écrans de moins de 768px de large (la plupart des 
mobiles)
- Intégration des notions de PWA (Progressive Web App) permettant d'installer le site en tant qu'application
sur un téléphone/tablette. (Gestion du fichier manifest, installation en mode standalone). Nous avons voulu 
intégrer Workbox afin de gérer les ressources dans le cache du navigateur mais les framework étant interdits, nous
n'avons pas pris la liberté de l'implémenter.
- Thème sombre/clair totalement intégré, avec persistance des choix de l'utilisateur via le stockage local du
navigateur du client
- Développement du projet en respectant le mode de développement MVC (Modèle, Vue, Contrôleur)
- Gestion de projet sur la plate-forme GitLab, avec le protocole Git. Structuration du code avec des Pull Requests, 
des tickets à chaque bug ou fonctionnalité à implémenter. Suivi avec des messages de commit représentatifs. Nous avons appliqué 
à la lettre les principes de gestion de projet utilisés en entreprise concernant le versioning Git. Vous pouvez accéder à tous nos tickets que nous avons terminé
[ici](https://gitlab.com/M0ustach3/bookstore/issues?scope=all&utf8=%E2%9C%93&state=closed)
- Création de PHPDoc sur les classes et fonctions afin d'optimiser la maintenabilité du projet
- Développement orienté composants afin de réutiliser le code
- Respect des règles de Material Design afin de rendre le site plus agréable à l'oeil
- Utilisation des nouvelles fonctionnalités de CSS3 (variables, entre autres)
- Respect des icônes Material pour garder une cohésion dans le site
- Affichage des produits via une animation délayée
- Validation des champs de création d'utilisateur via JS, et côté serveur via PHP