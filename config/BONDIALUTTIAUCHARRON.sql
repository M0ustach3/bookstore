-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  ven. 24 jan. 2020 à 17:25
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bondialuttiaucharron`
--
CREATE DATABASE IF NOT EXISTS `bondialuttiaucharron` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bondialuttiaucharron`;

-- --------------------------------------------------------

--
-- Structure de la table `author`
--

DROP TABLE IF EXISTS `author`;
CREATE TABLE `author` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `author`
--

INSERT INTO `author` (`id`, `name`, `surname`) VALUES
(1, 'Fred', 'Vargas'),
(2, 'Donato', 'Carrisi'),
(3, 'Jim', 'Thompson'),
(4, 'Harlan', 'Coben'),
(5, 'Robert Louis', 'Stevenson'),
(6, 'Jack', 'London'),
(7, 'Herman', 'Melville'),
(8, 'Les Nuls', ''),
(9, 'Jean-Philippe', 'Jaworski'),
(10, 'Daniel', 'Keyes'),
(11, 'Orson Scott', 'Card'),
(12, 'John', 'Green'),
(13, 'Jane', 'Austen'),
(14, 'Emma', 'Quinn'),
(15, 'Maria', 'Snyder'),
(16, 'Fiona', 'McIntosh'),
(17, 'Markus', 'Heitz'),
(18, 'Alice', 'Greetham'),
(19, 'Steven', 'Raichlen');

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`, `logo`) VALUES
(1, 'Policier', 'policier.jpg'),
(2, 'Science-Fiction', 'sf.jpg'),
(3, 'Aventure', 'aventure.jpg'),
(4, 'Sentimental', 'sentimental.jpg'),
(5, 'Fantastique', 'fantastique.jpg'),
(6, 'Vie pratique', 'viePratique.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `command`
--

DROP TABLE IF EXISTS `command`;
CREATE TABLE `command` (
  `id` int(11) NOT NULL,
  `date` bigint(20) NOT NULL,
  `id_customer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commandproduct`
--

DROP TABLE IF EXISTS `commandproduct`;
CREATE TABLE `commandproduct` (
  `id` int(11) NOT NULL,
  `id_command` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `address` text,
  `postal_code` mediumint(9) DEFAULT NULL,
  `city` text,
  `birth` bigint(20) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` float NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `date` bigint(20) NOT NULL,
  `id_author` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `image`, `description`, `date`, `id_author`) VALUES
(1, 'Pars vite et reviens tard', 12.99, 'parsViteEtReviensTard.jpg', 'Alors qu\'un ancien marin breton, Joss Le Guern, connaît quelque succès en reprenant le vieux métier de crieur public, le commissaire Jean-Baptiste Adamsberg est alerté par une femme inquiète de la présence de grands « 4 » noirs inversés avec des barres et sous-titrés des trois lettres CLT (Cito Longe Tarde), qui veut dire « vite, loin et tard » (d\'où le titre), sur toutes les portes de son immeuble, à l\'exception d\'une seule. De plus en plus intrigué lorsqu\'un second immeuble subit le même sort, Adamsberg s\'alarme véritablement lorsque le crieur, épaulé par le vieil érudit Decambrais, vient lui rapporter des messages énigmatiques laissés par un inconnu. ', 978307200, 1),
(2, 'Le Chuchoteur', 15.87, 'leChuchoteur.jpg', 'Depuis qu’ils enquêtent sur les rapts des fillettes, le criminologue Goran Gavila et son équipe d’agents spéciaux ont l’impression d’être manipulés. Chaque découverte macabre, chaque indice les mènent à des assassins différents. La découverte d’un sixième bras, dans la clairière, appartenant à une victime inconnue, les convainc d’appeler en renfort Mila Vasquez, experte dans les affaires d’enlèvement. Dans le huis clos d’un appartement spartiate converti en QG, Gavila et ses agents vont échafauder une théorie à laquelle nul ne veut croire : tous les meurtres sont liés, le vrai coupable est ailleurs.\r\nQuand on tue des enfants, Dieu se tait, et le diable murmure…', 1230768000, 2),
(3, 'L’assassin qui est en moi', 8.99, 'LAssassinQuiEstEnMoi.jpg', 'Cela ne me viendrait pas à l\'idée de te menacer, Lou, mon chéri, mais je suis bien décidée à ne jamais renoncer à toi. Jamais, jamais, jamais. Si tu es trop bien pour moi, alors je ferai ce qu\'il faut pour que tu ne le sois plus. Je l\'embrasse - un long baiser, brutal. Car Joyce ne le sait pas, mais elle est déjà morte, et d\'une certaine façon, je ne pourrais pas l\'aimer davantage. Un classique de Jim Thompson dans une nouvelle traduction intégrale.', -568080000, 3),
(4, 'Ne le dis à personne', 9.44, 'neLeDisAPersonne.jpg', 'David Beck est un pédiatre qui adore son métier. Il l\'exerce avec passion dans une clinique qui prend en charge les enfants défavorisés. Sa femme, Elizabeth, qu\'il connait depuis l\'enfance, est assassinée par un tueur en série. Huit ans plus tard, David ne parvient toujours pas à tourner la page. C\'est alors qu\'il reçoit un mystérieux e-mail anonyme. Lorsqu\'il clique sur le lien contenu dans ce message, une image apparait. Stupéfait, David reconnait le visage de sa femme sur cette vidéo filmée en temps réel.\r\n\r\nAbasourdi, David se remémore les détails de l\'assassinat de son épouse, dont le propre père, officier de police, a formellement identifié le corps. Impatient, il guette le prochain message qui lui donne rendez-vous le lendemain. David va alors tout tenter pour faire éclater la vérité. ', 978307200, 4),
(5, 'L\'île au trésor', 9.99, 'lileAuTresor.jpg', 'Le jeune Jim Hawkins2 est le fils du gérant de l\'auberge « L\'Amiral-Benbow ». Un jour, un vieux loup de mer chargé d\'un coffre, et nommé Billy Bones, débarque à l\'auberge et s\'y installe. Jim est tout à la fois fasciné et terrifié par ce marin colérique, violent et ivrogne, d\'autant qu\'une obscure menace semble planer sur ce dernier. ', -2776982400, 5),
(6, 'L\'appel de la forêt', 12.56, 'appelDeLaForet.jpg', 'Buck est un chien de Californie qui appartient au juge Miller. Il est un jour enlevé à son maître par l\'aide-jardinier du juge et vendu à un trafiquant de chiens de traîneau. Bientôt confronté à la brutalité de sa nouvelle vie, Buck doit trouver la force de survivre et s’adapter au froid de l’Alaska et du Yukon devant s’imposer aux autres chiens de la meute, il apprend à voler de la viande ainsi qu’à se battre pour survivre. Il est très souvent vendu jusqu’à ce qu’il devienne le chien d’un maître respectable, John Thornton. Mais lorsque son maître est tué par une tribu indienne, Buck redevient un loup et tue une partie des assassins. Rendu à la nature au milieu du Wild, la grande forêt nord-canadienne, il se mêle à une meute de loups dont il devient le chef. ', -2114380800, 6),
(7, 'La LSF pour les nuls', 19.99, 'langueDesSignes.jpg', 'Tout d\'abord, les Nuls vous proposent un petit voyage découverte de la communauté sourde et de son histoire. Puis, grâce à une méthode progressive, vous vous familiariserez avec l\'alphabet, les chiffres, puis découvrirez le vocabulaire par grandes thématiques : nature, animaux, alimentation, transports, métiers, mais aussi rapport au temps et localisation dans l\'espace, avec le support de centaines de photos.', 1325419200, 8),
(8, 'Gagner la guerre', 12.9, 'gagnerLaGuerre.jpg', 'Gagner une guerre, c\'est bien joli, mais quand il faut partager le butin entre les vainqueurs, et quand ces triomphateurs sont des nobles pourris d\'orgueil et d\'ambition, le coup de grâce infligé à l\'ennemi n\'est qu\'un amuse-gueule. C\'est la curée qui commence. On en vient à regretter les bonnes vieilles batailles rangées et les tueries codifiées selon l\'art militaire. Désormais, pour rafler le pactole, c\'est au sein de la famille qu\'on sort les couteaux. Et il se trouve que les couteaux, justement, c\'est plutôt mon rayon...', 1425513600, 9),
(9, 'Des fleurs pour Algernon', 6.9, 'desFleursPourAlgernon.jpg', 'Algernon est une souris dont le traitement du Pr Nemur et du Dr Strauss vient de décupler l\'intelligence. Enhardis par cette réussite, les savants tentent, avec l\'assistance de la psychologue Alice Kinnian, d\'appliquer leur découverte à Charlie Gordon, un simple d\'esprit. C\'est bientôt l\'extraordinaire éveil de l\'intelligence pour le jeune homme. Il découvre un monde dont il avait toujours été exclu, et l\'amour qui naît entre Alice et lui achève de le métamorphoser. Mais un jour, les facultés supérieures d\'Algernon commencent à décliner...', 1345852800, 10),
(10, 'La stratégie Ender', 7.9, 'laStrategieEnder.jpg', 'Andrew Wiggin, dit Ender, n\'est pas un garçon comme les autres. Depuis sa naissance, ses faits et gestes sont observés par l\'intermédiaire d\'un moniteur greffé dans son cerveau. Car ceux qui l\'ont conçu ambitionnent de faire de lui le plus grand général de tous les temps, le seul capable de sauver ses semblables de l\'invasion des doryphores. Et alors qu\'Ender suit pas à pas le dur chemin de son apprentissage de guerrier, ses créateurs mesurent la gravité de leur choix : en donnant naissance à un monstre, n\'ont-ils pas damné l\'humanité elle-même ?', 1362009600, 11),
(11, 'Nos étoiles contraires', 15.9, 'nosEtoilesContraires.jpg', 'Depuis son enfance, Hazel a des problèmes respiratoires, l\'obligeant à porter un tube à oxygène en permanence. Sur les conseils de sa mère, elle participe à un groupe de soutien, où elle fait la connaissance d\'Augustus, qui a perdu une jambe à cause d\'un cancer. Charismatique et sûr de lui, le jeune homme devient rapidement proche d\'Hazel, lui montrant le bon côté des choses. Il arrive même à retrouver l\'écrivain préféré de sa nouvelle amie, planifiant un voyage à Amsterdam pour le rencontrer.', 1508371200, 12),
(13, 'Le bébé de mon Ex', 70.56, 'leBebeDeMonEx.jpg', 'Ca fait six ans, Brenda. Six ans depuis que je me suis enfuie loin de lui. Six ans depuis que je l’ai surpris au lit avec une autre.', 1354665600, 14),
(14, 'Le poison écarlate', 12.9, 'lePoisonEcarlate.jpg', 'Dans les geôles d\'Ixia, Elena attend son exécution. Mais, au dernier moment, le fascinant Valek, puissant dignitaire secrètement amoureux d\'elle, lui propose un étrange marché : si elle entre à son service, elle aura la vie sauve. Néanmoins, qu\'elle ne songe pas à s\'enfuir - car pour être certain de la retenir près de lui, Valek lui fera avaler une dose mortelle du poison écarlate, dont il est seul à connaître la formule et surtout l\'antidote.', 1339459200, 15),
(15, 'Le dernier souffle - Tome 1 : Le Don', 15.2, 'leDernierSouffle.jpg', 'Wyl Thirsk n’est encore qu’un adolescent lorsqu’il doit assumer le rôle qu’on lui destine depuis sa naissance : commandant en chef des armées de Morgravia. Une responsabilité qui le place sous les ordres d’un despote sadique. Un geste de bonté envers une sorcière condamnée au bûcher vaudra un jour à Wyl un don miraculeux, ainsi que la colère de son seigneur et maître. Wyl est alors envoyé au Nord, pour une mission suicidaire à la cour ennemie… avec pour seule arme un mystérieux pouvoir dont il ne soupçonne pas même l’existence. Or, s’il n’embrasse pas le Dernier Souffle, il signera sa perte et celle du pays qu’il a juré de défendre.', 1528848000, 16),
(16, 'Les Nains - L\'intégrale', 17.9, 'lesNains.jpg', 'Lorsque s\'effondre le passage de Pierre que les Nains gardaient depuis toujours, Orcs et Ogres déferlent sur le Pays Sûr. C\'est le jeune Nain Tungdil qui donne l\'alerte. Envoyé en mission par son père adoptif, le Mage Lot-Ionan, il découvre l\'armée qui avance sur le pays. À la tête de cette force d\'invasion, les Albes, êtres cruels et maléfiques, ont le pouvoir de ramener les morts à la vie. Tungdil n\'a pas d\'autre choix, s\'il veut sauver Hommes, Elfes, Mages et Nains du péril imminent, que de devenir un héros.', 1282867200, 17),
(17, 'Cuisine Vegan - Super Facile', 17.9, 'cuisineVegan.jpg', 'Découvrez les recettes super faciles d\'une cuisine végane tout en saveur et en couleur avec 90 recettes inédites qui, par leur simplicité, vous rendront la vie tellement plus facile et votre table tellement plus gourmande !', 1483574400, 18),
(18, 'Pros du fumoir', 46.3, 'prosDuFumoir.jpg', 'J\'aime la viande. La viande, c\'est bon. LOL.', 1518652800, 19);

-- --------------------------------------------------------

--
-- Structure de la table `productcategory`
--

DROP TABLE IF EXISTS `productcategory`;
CREATE TABLE `productcategory` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `productcategory`
--

INSERT INTO `productcategory` (`id`, `id_product`, `id_category`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 3),
(6, 6, 3),
(7, 7, 6),
(8, 17, 6),
(9, 9, 2),
(10, 8, 2),
(11, 10, 2),
(12, 13, 4),
(13, 15, 5),
(14, 14, 5),
(15, 16, 5),
(16, 11, 4),
(17, 18, 6);

-- --------------------------------------------------------

--
-- Structure de la table `rating`
--

DROP TABLE IF EXISTS `rating`;
CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `value` tinyint(4) NOT NULL,
  `comment` text NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_product` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `command`
--
ALTER TABLE `command`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_command_customer` (`id_customer`);

--
-- Index pour la table `commandproduct`
--
ALTER TABLE `commandproduct`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_commandproduct_command` (`id_command`),
  ADD KEY `fk_commandproduct_product` (`id_product`);

--
-- Index pour la table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_author` (`id_author`);

--
-- Index pour la table `productcategory`
--
ALTER TABLE `productcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_productcategory_product` (`id_product`),
  ADD KEY `fk_productcategory_category` (`id_category`);

--
-- Index pour la table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_rating_customer` (`id_customer`),
  ADD KEY `fk_rating_product` (`id_product`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `author`
--
ALTER TABLE `author`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `command`
--
ALTER TABLE `command`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `commandproduct`
--
ALTER TABLE `commandproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `productcategory`
--
ALTER TABLE `productcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `command`
--
ALTER TABLE `command`
  ADD CONSTRAINT `fk_command_customer` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id`);

--
-- Contraintes pour la table `commandproduct`
--
ALTER TABLE `commandproduct`
  ADD CONSTRAINT `fk_commandproduct_command` FOREIGN KEY (`id_command`) REFERENCES `command` (`id`),
  ADD CONSTRAINT `fk_commandproduct_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_author` FOREIGN KEY (`id_author`) REFERENCES `author` (`id`);

--
-- Contraintes pour la table `productcategory`
--
ALTER TABLE `productcategory`
  ADD CONSTRAINT `fk_productcategory_category` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `fk_productcategory_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);

--
-- Contraintes pour la table `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `fk_rating_customer` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `fk_rating_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
