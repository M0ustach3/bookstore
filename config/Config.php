<?php
/**
 * Fichier de configuration pour la base de données
 */


/**
 * Class Config utilisée pour les identifiants de connexion à la base de données.
 * Cette classe est utilisée par la classe {@link Model}
 *
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class Config
{
    /**
     * Constante utilisée pour l'hôte
     */
    const HOST = 'localhost';
    /**
     * Constante utilisée pour l'utilisateur de connexion à la base de données
     */
    const USER = 'pablo';
    /**
     * Mot de passe utilisé pour l'utilisateur spécifié dans la constante HOST
     */
    const PASSWORD = 'pablo';
    /**
     * La base de données à utiliser
     * NE PAS MODIFIER CETTE VALEUR SI VOUS UTILISEZ LE SCRIPT SQL FOURNI DANS config/
     */
    const DATABASE = 'bondialuttiaucharron';
    /**
     * Le port de la base de données
     */
    const PORT = 3306;
    /**
     * Le protocole utilisé par PDO pour se connecter à la base de données
     */
    const PROTOCOL = "mysql";
}