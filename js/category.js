let books = Array.prototype.slice.call(document.querySelectorAll("#productsInCategory > a"));

document.getElementsByClassName("fab-buttons__item")[0].addEventListener('click', function (event) {
    books.sort(function (first, second) {
        return first.children[0].children[1].children[0].children[0].textContent.localeCompare(second.children[0].children[1].children[0].children[0].textContent);
    });
    for (let i = 0, len = books.length; i < len; i++) {
        let parent = books[i].parentNode;
        let detatchedItem = parent.removeChild(books[i]);
        parent.appendChild(detatchedItem);
    }

});


document.getElementsByClassName("fab-buttons__item")[1].addEventListener('click', function (event) {
    books.sort(function (first, second) {
        return first.children[0].children[1].children[0].children[1].textContent.localeCompare(second.children[0].children[1].children[0].children[1].textContent);
    });
    for (let i = 0, len = books.length; i < len; i++) {
        let parent = books[i].parentNode;
        let detatchedItem = parent.removeChild(books[i]);
        parent.appendChild(detatchedItem);
    }

});