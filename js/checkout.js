function hideMethods() {
    let childrenToHide = document.querySelectorAll('#checkout > div:nth-child(3) > div:nth-child(2) > div');
    childrenToHide.forEach(function (item, index) {
        item.children[1].style.display = "none";
    });
}

function hidePanels() {
    let divsToHide = document.querySelectorAll(".checkoutCategoryHeader + div");
    divsToHide.forEach(function (item, index) {
        item.style.display = "none";
    });
}

hidePanels();
hideMethods();
document.querySelectorAll('#checkout > div:nth-child(3) > div:nth-child(2) > div').forEach(function (item, index) {
    item.children[0].addEventListener('mouseup', function (event) {
        hideMethods();
        item.children[1].style.display = "flex";
    });
});

document.querySelectorAll(".checkoutCategoryHeader").forEach(function (item, index) {
    item.addEventListener("click", function (event) {
        if (item.nextElementSibling.style.display === "") {
            item.nextElementSibling.style.display = "none";
            item.children[1].children[0].innerText = "unfold_more";
        } else {
            item.nextElementSibling.style.display = null;
            item.children[1].children[0].innerText = "unfold_less";
        }
    });
});