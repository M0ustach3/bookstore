window.addEventListener('DOMContentLoaded', (event) => {

    if (typeof (Storage) !== "undefined") {
        if (window.localStorage.getItem('darkThemeEnabled') === null) {
            window.localStorage.setItem('darkThemeEnabled', '0');
            location.reload();
        } else if (window.localStorage.getItem('darkThemeEnabled') === '1') {
            document.documentElement.style.setProperty("--primary-color", "#212121");
            document.documentElement.style.setProperty("--background-color", "#424242");
            document.documentElement.style.setProperty("--secondary-color", "#2196f3");
            document.documentElement.style.setProperty("--accent-color", "white");
            document.documentElement.style.setProperty("--fadein-secondary-color", "#64B5F6");
            document.getElementById("change").checked = "true";
        } else {
            document.documentElement.style.setProperty("--secondary-color", "#ffa726");
            document.documentElement.style.setProperty("--primary-color", "#ba68c8");
            document.documentElement.style.setProperty("--background-color", "white");
            document.documentElement.style.setProperty("--accent-color", "black");
            document.documentElement.style.setProperty("--fadein-secondary-color", "#ffd95b");
            document.getElementById("change").removeAttribute("checked");
        }

        document.getElementById("change").addEventListener('change', function (event) {
            if (window.localStorage.getItem('darkThemeEnabled') === '0') {
                window.localStorage.setItem('darkThemeEnabled', '1');
            } else {
                window.localStorage.setItem('darkThemeEnabled', '0');
            }
            location.reload();
        });
    }
});