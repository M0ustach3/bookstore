document.querySelectorAll(".login-form > input").forEach((item, index) => {
    item.addEventListener("focus", function (event) {
        event.target.style.backgroundColor = "var(--secondary-color)";
    });
    item.addEventListener("blur", function (event) {
        event.target.style.backgroundColor = "var(--background-color)";
    });
});

document.querySelector(".login-form > input:nth-child(4)").addEventListener("keyup", function (event) {
    if (isNaN(event.target.value)) {
        event.target.style.backgroundColor = "var(--error-color)";
    } else {
        event.target.style.backgroundColor = "var(--secondary-color)";
    }
});

document.querySelector(".login-form").addEventListener("submit", function (event) {
    let createUserForm = document.forms["createUserForm"];
    let zipcode = createUserForm["zipcode"];
    let date = createUserForm["birth"];

    if (isNaN(zipcode.value)) {
        zipcode.style.backgroundColor = "var(--error-color)";
        event.preventDefault();
    }
});