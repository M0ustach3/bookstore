<?php
session_start();
require_once "controllers/Router.php";

include "includes/header.php";
$router = new Router();
$router->routeRequest();
include "includes/footer.php";
