<?php
require_once "config/Config.php";

/**
 * Représente la connexion à la BDD
 */

/**
 * Class Model représentant les méthodes d'interaction avec la base de données
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class Model
{
    /**
     * @var PDO L'objet PDO représentant la connexion à la base de données
     */
    private $conn;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        try {
            $string = Config::PROTOCOL . ":host=" . Config::HOST . ":" . Config::PORT . ";dbname=" . Config::DATABASE;
            $this->conn = new PDO($string, Config::USER, Config::PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /**
     * Méthode utilisée pour récupérer des livres aléatoires
     * @param $limit string le nombre maximum de livres
     * @return array des livres aléatoires
     */
    public function getRandomBooks($limit)
    {
        $query = "SELECT BONDIALUTTIAUCHARRON.Product.name AS productName,
       BONDIALUTTIAUCHARRON.Product.id AS productID,
       BONDIALUTTIAUCHARRON.Category.name AS categoryName,
       BONDIALUTTIAUCHARRON.Author.name AS authorName,
       BONDIALUTTIAUCHARRON.Author.surname AS authorSurname,
       CONCAT('images/covers/', Product.image)     AS productPath,
       CONCAT('images/categories/', Category.logo) AS categoryPath
FROM ((BONDIALUTTIAUCHARRON.Product
    INNER JOIN BONDIALUTTIAUCHARRON.ProductCategory ON Product.id = ProductCategory.id_product)
         INNER JOIN Category ON Category.id = ProductCategory.id_category),
     BONDIALUTTIAUCHARRON.Author
WHERE BONDIALUTTIAUCHARRON.Product.id_author = BONDIALUTTIAUCHARRON.Author.id
ORDER BY RAND() LIMIT ?";
        $statement = $this->conn->prepare($query);
        $sanitized = htmlspecialchars($limit);
        $statement->bindParam(1, $sanitized, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * Méthode utilisée pour récupérer des catégories
     * @param $limit string le nombre maximum de catégories
     * @return array les catégories récupérées
     */
    public function getCategories($limit)
    {
        $query = "SELECT name, CONCAT('images/categories/', logo) AS path, id
FROM BONDIALUTTIAUCHARRON.Category
ORDER BY BONDIALUTTIAUCHARRON.Category.name LIMIT ?;";
        $statement = $this->conn->prepare($query);
        $sanitized = htmlspecialchars($limit);
        $statement->bindParam(1, $sanitized, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * Méthode utilisée pour valider si une combinaison utilisateur/mdp est valide
     * @param $user string le nom d'utilisateur
     * @param $password string le mot de passe
     * @return bool true si l'utilisateur est valide, false sinon
     */
    public function isUserValid($user, $password)
    {
        $sanitizedUser = htmlspecialchars($user);
        $state = $this->conn->prepare("SELECT password FROM Customer WHERE username=:username");
        $state->execute(array("username" => $sanitizedUser));
        if ($userdata = $state->fetch()) {
            return password_verify($password, $userdata['password']);
        } else {
            return false;
        }
    }

    /**
     * Méthode utilisée pour valider si un utilisateur existe
     * @param $username string le nom d'utilisateur à tester
     * @return bool true si l'utilisateur existe déjà dans la base de données, false sinon
     */
    public function doesUserExists($username)
    {
        $sanitizedUser = htmlspecialchars($username);
        $state = $this->conn->prepare("SELECT id FROM Customer WHERE username=:username");
        $state->execute(array("username" => $sanitizedUser));
        return count($state->fetchAll()) > 0;
    }

    /**
     * Méthode utilisée pour créer un utilisateur
     * @param $surname string le nom de famille
     * @param $name string le prénom
     * @param $address string l'adresse
     * @param $zipcode string le code postal
     * @param $city string la ville
     * @param $birth string la date de naissance
     * @param $user string le nom d'utilisateur
     * @param $password string le mot de passe
     * @return string "success" si la création à réussi, "userExists" si l'utilisateur existe déjà, "noPasswdOfUser"
     * si l'un des deux champs obligatoires n'est pas rempli
     */
    public function createUser($surname, $name, $address, $zipcode, $city, $birth, $user, $password)
    {
        if ($this->doesUserExists($user)) {
            return "userExists";
        } else if (empty($user) || empty($password)) {
            return "noPasswdOrUser";
        } else {
            $sanitized = array(
                "surname" => !empty(htmlspecialchars($surname)) ? htmlspecialchars($surname) : null,
                "name" => !empty(htmlspecialchars($name)) ? htmlspecialchars($name) : null,
                "address" => !empty(htmlspecialchars($address)) ? htmlspecialchars($address) : null,
                "postal_code" => is_numeric(htmlspecialchars($zipcode)) ? htmlspecialchars($zipcode) : null,
                "city" => !empty(htmlspecialchars($city)) ? htmlspecialchars($city) : null,
                "birth" => !empty(htmlspecialchars($birth)) ? strtotime(htmlspecialchars($birth)) : null,
                "user" => htmlspecialchars($user),
                "password" => password_hash($password, PASSWORD_BCRYPT),
            );
            $statement = $this->conn->prepare("INSERT INTO Customer(id, name, surname, address, postal_code, city, birth, username, password)
            VALUES (NULL, :name,:surname,:address,:postal_code,:city,:birth,:user,:password)");
            $statement->execute($sanitized);
            return "success";
        }
    }

    /**
     * Méthode utilisée pour récupérer les informations à partir d'un nom d'utilisateur
     * @param $user string le nom d'utilisateur
     * @return mixed un tableau représentant les données de l'utilisateur ou false
     */
    public function getUserInfo($user)
    {
        $statement = $this->conn->prepare("SELECT id, name, surname, address, postal_code, city, birth, username FROM Customer WHERE username=:username");
        $statement->execute(array("username" => htmlspecialchars($user)));
        return $statement->fetch();
    }

    /**
     * Méthode utilisée pour récupérer les commandes de l'utilisateur
     * @param $user string le nom d'utilisateur
     * @return array les commandes de l'utilisateur
     */
    public function getUserCommands($user)
    {
        $userID = $this->getUserInfo($user)['id'];
        $statement = $this->conn->prepare("SELECT c.id, c.date AS commandDate, id_customer,p.id, name, price, image, description, CP.quantity
FROM Command c
         INNER JOIN CommandProduct CP on c.id = CP.id_command
         INNER JOIN Product P on CP.id_product = P.id
WHERE c.id_customer = :idCustomer ORDER BY c.date DESC;");
        $statement->execute(array("idCustomer" => htmlspecialchars($userID)));
        return $statement->fetchAll();
    }

    /**
     * Méthode utilisée pour récupérer les produits non notés
     * @param $user string le nom d'utilisateur
     * @return array tableau représentant les produits non notés
     */
    public function getNonRatedProducts($user)
    {
        $userID = $this->getUserInfo($user)['id'];
        $statement = $this->conn->prepare("SELECT DISTINCT P.id AS bookID,
                P.name    AS productName,
                price,
                image,
                description,
                P.date,
                id_author,
                A.id AS authorID,
                A.name    AS authorName,
                A.surname
FROM CommandProduct
         INNER JOIN Command C on CommandProduct.id_command = C.id
         INNER JOIN Product P on CommandProduct.id_product = P.id
         INNER JOIN Author A on P.id_author = A.id
WHERE id_product NOT IN (SELECT Rating.id_product
                         FROM Rating
                         WHERE id_customer = :idUser)
  AND C.id_customer = :idUser");
        $statement->execute(array("idUser" => htmlspecialchars($userID)));
        return $statement->fetchAll();
    }

    /**
     * Méthode utilisée pour récupérer les détails d'un produit
     * @param $idProduct string l'ID d'un produit
     * @return array tableau représentant les détails d'un produit
     */
    public function getProductDetails($idProduct)
    {
        $statement = $this->conn->prepare("SELECT Product.id AS bookID,
       Product.name  AS productName,
       price,
       image,
       description,
       date,
       Author.id,
       Author.name   AS authorName,
       surname,
       ipm.average,
       Category.name AS categoryName
FROM ((Product
    LEFT JOIN (SELECT id_product, AVG(value) AS average
               FROM Rating
               GROUP BY id_product) as ipm ON ipm.id_product = Product.id)
         INNER JOIN Author ON Product.id_author = Author.id),
     Category,
     ProductCategory
WHERE Product.id = :idProduct
  AND Product.id = ProductCategory.id_product");
        $statement->execute(array("idProduct" => htmlspecialchars($idProduct)));
        $statement2 = $this->conn->prepare("SELECT *
FROM Rating
         INNER JOIN Customer C on Rating.id_customer = C.id
WHERE id_product=:idProduct");
        $statement2->execute(array("idProduct" => htmlspecialchars($idProduct)));
        $statement3 = $this->conn->prepare("SELECT name
FROM Category
         INNER JOIN ProductCategory PC on Category.id = PC.id_category
WHERE PC.id_product = :idProduct");
        $statement3->execute(array("idProduct" => htmlspecialchars($idProduct)));
        return array(
            "productInfo" => $statement->fetch(),
            "productComments" => $statement2->fetchAll(),
            "productCategories" => $statement3->fetchAll()
        );
    }

    /**
     * Méthode utilisée pour récupérer les produits d'une catégorie
     * @param $categoryID string l'ID d'une catégorie
     * @return array tableau représentant les produits de la catégorie
     */
    public function getProductsByCategoryID($categoryID)
    {
        $statement = $this->conn->prepare("SELECT Product.*,  Author.name AS authorName, Author.surname
FROM ((Product
    INNER JOIN ProductCategory PC on Product.id = PC.id_product)
         INNER JOIN Author ON id_author = Author.id)
WHERE id_category = :idCategory");
        $statement->execute(array("idCategory" => htmlspecialchars($categoryID)));
        return $statement->fetchAll();
    }

    /**
     * Méthode utilisée pour récupérer les produits filtrés sur certains critères
     * @param $getParameters array les paramètres de filtrage
     * @return array les produits correspondant aux filtres fournis
     */
    public function filterBooks($getParameters)
    {
        $queryString = "SELECT Product.id,
       Product.name  AS productName,
       price,
       image,
       description,
       date,
       author.id,
       Author.name   AS authorName,author.surname FROM product,author  WHERE product.id_author=author.id";

        // DATE FILTERING
        // Both low and high are set
        if (!empty($getParameters['yearLow']) && !empty($getParameters['yearHigh'])) {
            $dateTimestampLow = strtotime("00:00 Jan 01 " . $getParameters['yearLow']);
            $dateTimestampHigh = strtotime("00:00 Jan 01 " . $getParameters['yearHigh']);
            $queryString .= " AND date >= " . $dateTimestampLow . " AND date <= " . $dateTimestampHigh;
            //Only low is set
        } else if (!empty($getParameters['yearLow']) && empty($getParameters['yearHigh'])) {
            $dateTimestampLow = strtotime("00:00 Jan 01 " . $getParameters['yearLow']);
            $queryString .= " AND date >= " . $dateTimestampLow;
            //Only high is set
        } else if (empty($getParameters['yearLow']) && !empty($getParameters['yearHigh'])) {
            $dateTimestampHigh = strtotime("00:00 Jan 01 " . $getParameters['yearHigh']);
            $queryString .= " AND date <= " . $dateTimestampHigh;
        }

        // PRICE FILTERING
        // Both low and high are set
        if (!empty($getParameters['priceLow']) && !empty($getParameters['priceHigh'])) {
            $queryString .= " AND price >= " . $getParameters['priceLow'] . " AND price <= " . $getParameters['priceHigh'];
            //Only low is set
        } else if (!empty($getParameters['priceLow']) && empty($getParameters['priceHigh'])) {
            $queryString .= " AND price >= " . $getParameters['priceLow'];
            //Only high is set
        } else if (empty($getParameters['priceLow']) && !empty($getParameters['priceHigh'])) {
            $queryString .= " AND price <= " . $getParameters['priceHigh'];
        }

        //AUTHOR FILTERING
        if (!empty($getParameters['authorSearch']) && $getParameters['authorSearch'] !== "*none*") {
            $queryString .= " AND id_author=" . $getParameters['authorSearch'];
        }

        //CATEGORY FILTERING
        if (!empty($getParameters['categorySearch']) && $getParameters['categorySearch'] !== "*none*") {
            $queryString .= " AND product.id IN (SELECT DISTINCT id FROM productcategory WHERE id_category=" . $getParameters['categorySearch'] . ")";
        }


        $statement = $this->conn->prepare($queryString);
        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * Méthode utilisée pour rechercher des livres selon leur titre
     * @param $searchTerm string le terme à rechercher
     * @return array tableau représentant les produits contenant le terme de recherche
     */
    public function getBooksBySearchTerm($searchTerm)
    {
        $statement = $this->conn->prepare("SELECT Product.id AS bookID,
       Product.name  AS productName,
       price,
       image,
       description,
       date,
       author.id AS authorID,
       Author.name   AS authorName,author.surname FROM product,author  WHERE product.id_author=author.id AND product.name LIKE :term");
        $statement->execute(array(
            "term" => '%' . strtolower($searchTerm) . '%'
        ));
        return $statement->fetchAll();
    }

    /**
     * Méthode utilisée pour récupérer les différents auteurs de la base de données
     * @return array tableau représentant les différents auteurs
     */
    public function getAuthors()
    {
        $statement = $this->conn->prepare("SELECT DISTINCT * FROM Author ORDER BY Author.surname ");
        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * Méthode utilisée pour créer une commande
     * @param $userID string l'id de l'utilisateur
     * @param $cart array le panier de l'utilisateur
     * @throws Exception renvoyée par lastInsertId si une erreur survient lors de la récupération du dernier id inséré
     */
    public function createCommand($userID, $cart)
    {
        $date = (new DateTime())->getTimestamp();

        $command = $this->conn->prepare("INSERT INTO command VALUES (NULL,:date,:customer)");
        $command->execute(array(
            "date" => $date,
            "customer" => htmlspecialchars($userID)
        ));

        $lastCommandId = $this->conn->lastInsertId();
        $productsIDs = array_keys($cart);

        $commandProduct = $this->conn->prepare("INSERT INTO commandproduct VALUES (NULL,:commandID,:productID,:quantity)");

        foreach ($productsIDs as $idProduct) {
            $commandProduct->execute(array(
                "commandID" => $lastCommandId,
                "productID" => $idProduct,
                "quantity" => $cart[$idProduct]
            ));
        }
    }

    /**
     * Méthode utilisée pour noter un produit
     * @param $bookID string l'id du produit à noter
     * @param $rating string la note du produit
     * @param $comment string le commentaire du produit
     * @param $user string username de l'utilisateur
     * @return bool true si le produit à bien été noté, false sinon
     */
    public function rateProduct($bookID, $rating, $comment, $user)
    {
        $userID = $this->getUserInfo($user)['id'];
        if (empty($bookID) || !isset($rating) || empty($comment) || empty($userID)) return false;
        $statement = $this->conn->prepare("INSERT INTO Rating VALUES (NULL,:rating,:comment,:customer,:bookID)");
        $statement->execute(array(
            "rating" => htmlspecialchars($rating),
            "comment" => htmlspecialchars($comment),
            "customer" => htmlspecialchars($userID),
            "bookID" => htmlspecialchars($bookID)
        ));
        return true;
    }

    /**
     * Méthode utilisée pour récupérer les catégories contenant le plus de produits
     * @param $limit string la limite de catégories
     * @return array les catégories triées par nombre de produits
     */
    public function getTopCategories($limit)
    {
        $statement = $this->conn->prepare("SELECT COUNT(p.id_product) AS totalBooks, category.name, category.id
FROM category
         INNER JOIN productcategory p on category.id = p.id_category
         INNER JOIN product p2 on p.id_product = p2.id
GROUP BY category.name, Category.id
ORDER BY totalBooks DESC
LIMIT ?");
        $sanitized = (int)htmlspecialchars($limit);
        $statement->bindParam(1, $sanitized, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * Destructor of Model
     */
    public function __destruct()
    {
        $this->conn = null;
    }
}