<?php
require_once 'View.php';
require_once 'views/components/Chip.php';
require_once 'views/components/CommentCard.php';
require_once "views/components/SnackBar.php";

/**
 * Classe viewproduct
 */

/**
 * Class ViewProduct représente la vue d'un produit
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ViewProduct implements View
{
    /**
     * Méthode utilisée pour afficher un produit
     * @param mixed $data les données à afficher
     * @return mixed|void
     */
    public function render($data)
    {
        ?>
        <div id="containerProduct">
            <?php
            if (is_array($data)) {
                ?>
                <div id="productDetails">
                    <div><img src="images/covers/<?= $data['productInfo']['image'] ?>"></div>
                    <div>
                        <div>
                            <div>
                                <div>
                                    <h1><?= $data['productInfo']['productName'] ?>
                                        (<?= date('Y', $data['productInfo']['date']) ?>)</h1>
                                    <?php
                                    foreach ($data['productCategories'] as $category) {
                                        Chip::generate($category['name']);
                                    }
                                    ?>
                                </div>
                                <div>
                                    <h3><?= $data['productInfo']['authorName'] . " " . strtoupper($data['productInfo']['surname']) ?></h3>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <h2>
                                        <?php
                                        if ((int)$data['productInfo']['average'] === 0) {
                                            echo 'Non noté';
                                        } else {
                                            for ($i = 0; $i < round($data['productInfo']['average'], 1); $i++) {
                                                echo '<i class="material-icons">star</i>';
                                            }
                                            for ($i = 0; $i < 5 - round($data['productInfo']['average'], 1); $i++) {
                                                echo '<i class="material-icons">star_border</i>';
                                            }
                                        }
                                        ?>
                                    </h2>
                                </div>
                                <div><span><?= explode(".", $data['productInfo']['price'])[0] ?></span>
                                    .<?= explode(".", $data['productInfo']['price'])[1] ?>€
                                </div>
                                <div>
                                    <form action="index.php" method="get">
                                        <div>
                                            <div>
                                                <button type="submit" name="add" value="-1"><i class="material-icons">
                                                        remove_shopping_cart
                                                    </i></button>
                                            </div>
                                            <div>
                                                <input name="addProduct" hidden
                                                       value="<?= $_GET['viewProduct'] ?>"/>
                                                <?= isset($_SESSION['cart'][$_GET['viewProduct']]) ? $_SESSION['cart'][$_GET['viewProduct']] : 0 ?>
                                            </div>
                                            <div>
                                                <button type="submit" name="add" value="1"><i class="material-icons">
                                                        add_shopping_cart
                                                    </i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <blockquote cite="Résumé">
                            <?= $data['productInfo']['description'] ?>
                        </blockquote>
                    </div>
                </div>
                <div id="productComments">
                    <?php
                    foreach ($data['productComments'] as $productComment) {
                        CommentCard::generate($productComment);
                    }
                    ?>
                </div>
                <?php
            } else {
                ?>
                <div id="noProducts">
                    <?= $data ?>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
        if (isset($_GET['added'])) {
            SnackBar::generate(array('type' => 'success', 'data' => 'Le livre à bien été ajouté au panier !'));
        } else if (isset($_GET['removed'])) {
            SnackBar::generate(array('type' => 'success', 'data' => 'Le livre à bien été retiré du panier !'));
        }
    }
}