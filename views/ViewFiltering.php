<?php
require_once "views/View.php";

/**
 * Classe viewfiltering
 */

/**
 * Class ViewFiltering vue représentant la page de résultats de filtrage
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ViewFiltering implements View
{
    /**
     * Méthode utilisée pour afficher la vue des produits filtrés
     * @param mixed $data les données à afficher
     * @return mixed|void
     */
    public function render($data)
    {
        ?>
        <div id="filterPanel" class="hidden">
            <?php
            Filter::generate('');
            ?>
        </div>
        <div id="filtering">
            <div id="fabFiltering" class="fab"><i class="material-icons">filter_list</i></div>
            <?php
            if (empty($data)) {
                echo '<h1>Aucun produit disponible pour ces critères</h1>';
            } else {
                ?>
                <h2>Résultats de la recherche</h2>
                <?php
                foreach ($data as $book) {
                    LongBookCard::generate($book);
                }
            }
            ?>
            <script src="js/filter.js" type="text/javascript"></script>
        </div>
        <?php
    }
}