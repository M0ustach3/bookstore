<?php
/**
 * Interface de composant
 */

/**
 * Interface Component représente un composant réutilisable
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
interface Component
{
    /**
     * Méthode permettant de générer le contenu du composant
     * @param $data mixed les données à afficher
     * @return mixed ne retourne rien dans toutes les utilisations dans le projet
     */
    public static function generate($data);
}