<?php
require_once 'views/components/Component.php';

/**
 * Composant bookcard
 */

/**
 * Class BookCard représente une carte de livre sur la page d'accueil
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class BookCard implements Component
{
    /**
     * Méthode de génération du composant
     * @param mixed $data les données à afficher
     * @return mixed|void ne renvoie rien
     */
    public static function generate($data)
    {
        echo "
        <a href='index.php?viewProduct=" . $data['data']['productID'] . "' style='animation: 2s ease " . $data['delay'] . "s normal backwards 1 fadein'>
        <div class='cardBook'>
            <img src='" . $data['data']['productPath'] . "'>
                <div class='infosCardBook'>
                    <h4>
                        " . $data['data']['productName'] . "
                       </h4>
                    <p>
                        " . $data['data']['authorName'] . " " . $data['data']['authorSurname'] . "
                     </p>
                </div>
        </div>
        </a>
        ";
    }
}