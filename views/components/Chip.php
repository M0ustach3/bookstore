<?php
require_once 'views/components/Component.php';

/**
 * Composant Chip
 */

/**
 * Class Chip représente une "chip" sur la page des produits
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class Chip implements Component
{
    /**
     * Méthode de génération du composant
     * @param mixed $data les données à afficher
     * @return mixed|void ne renvoie rien
     */
    public static function generate($data)
    {
        ?>
        <div class="chip">
            <?= $data ?>
        </div>
        <?php
    }
}