<?php
require_once "views/components/Component.php";
require_once "models/Model.php";

/**
 * Classe filter
 */

/**
 * Class Filter représente le bandeau de filtre
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class Filter implements Component
{
    /**
     * Méthode de génération du composant
     * @param mixed $data les données à afficher
     * @return mixed|void ne renvoie rien
     */
    public static function generate($data)
    {
        $db = new Model();
        $authors = $db->getAuthors();
        $categories = $db->getCategories(1000);
        $db->__destruct();
        ?>
        <h1 id="searchTitle">Recherche avancée</h1>
        <form action="index.php" method="get">
            <input name="advancedSearch" hidden>
            <div id="filter">
                <div>
                    <label>
                        Année min.
                        <select name="yearLow">
                            <option value="" selected>-</option>
                            <?php
                            for ($i = (int)date('Y'); $i >= 1500; $i--) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            }
                            ?>
                        </select>
                    </label>
                </div>
                <div class="delimiterFilter"></div>
                <div>
                    <label>
                        Année max.
                        <select name="yearHigh">
                            <option value="" selected>-</option>
                            <?php
                            for ($i = (int)date('Y'); $i >= 1500; $i--) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            }
                            ?>
                        </select>
                    </label>
                </div>
                <div class="delimiterFilter"></div>
                <div>
                    <label>
                        Prix min.
                        <select name="priceLow">
                            <option value="" selected>-</option>
                            <?php
                            for ($i = 50; $i >= 0; $i--) {
                                echo '<option value="' . $i . '">' . $i . '€</option>';
                            }
                            ?>
                        </select>
                    </label>
                </div>
                <div class="delimiterFilter"></div>
                <div>
                    <label>
                        Prix max.
                        <select name="priceHigh">
                            <option value="" selected>-</option>
                            <?php
                            for ($i = 50; $i >= 0; $i--) {
                                echo '<option value="' . $i . '">' . $i . '€</option>';
                            }
                            ?>
                        </select>
                    </label>
                </div>
                <div class="delimiterFilter"></div>
                <div>
                    <label>
                        Auteur
                        <select name="authorSearch">
                            <option value="*none*" selected>-</option>
                            <?php
                            foreach ($authors as $author) {
                                echo '<option value="' . $author['id'] . '">' . strtoupper($author['surname']) . ' ' . $author['name'] . '</option>';
                            }
                            ?>
                        </select>
                    </label>
                </div>
                <div class="delimiterFilter"></div>
                <div>
                    <label>
                        Catégorie
                        <select name="categorySearch">
                            <option value="*none*" selected>-</option>
                            <?php
                            foreach ($categories as $category) {
                                echo '<option value="' . $category['id'] . '">' . $category['name'] . '</option>';
                            }
                            ?>
                        </select>
                    </label>
                </div>
                <div>
                    <input type="submit" value="Rechercher">
                </div>
            </div>
        </form>
        <?php
    }
}