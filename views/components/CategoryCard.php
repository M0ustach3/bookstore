<?php
require_once 'views/components/Component.php';
/**
 * Composant categorycard
 */

/**
 * Class CategoryCard représente une carte de catégorie sur la page d'accueil
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class CategoryCard implements Component
{
    /**
     * Méthode de génération du composant
     * @param mixed $data les données à afficher
     * @return mixed|void ne renvoie rien
     */
    public static function generate($data)
    {
        ?>
        <a href="index.php?category=<?= $data['id'] ?>">
            <div class='categoryCard'>
                <img src='<?= $data['path'] ?>'>
                <div><?= $data['name'] ?></div>
            </div>
        </a>
        <?php
    }
}