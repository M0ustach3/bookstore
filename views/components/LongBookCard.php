<?php
require_once "views/components/Component.php";

/**
 * Composant longbookcard
 */

/**
 * Class LongBookCard représente une carte de livre avec plus ou moins de détails
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class LongBookCard implements Component
{
    /**
     * Méthode utilisée pour tronquer une chaîne de caractères
     * @param $string string la chaîne à découper
     * @param $limit string le nombre de caractères à garder
     * @return string la nouvelle chaîne tronquée
     */
    private static function truncate($string, $limit)
    {
        if (strlen($string) <= $limit) return $string;
        return substr($string, 0, $limit) . "...";
    }

    /**
     * Méthode de génération du composant
     * @param mixed $data les données à afficher
     * @return mixed|void ne renvoie rien
     */
    public static function generate($data)
    {
        ?>
        <?= !isset($data['toBeRated']) ? "<a href=\"index.php?viewProduct=" . (isset($data['id']) ? $data['id'] : $data['bookID']) . " \"> " : "" ?>
        <div class="longBookCard animate-bottom"
             <?= isset($data['delay']) ? "style='animation-delay: " . $data['delay'] . "s'" : "" ?>>
            <div>
                <img src="images/covers/<?= $data['image'] ?>">
            </div>
            <div>
                <div><span><?= isset($data['name']) ? $data['name'] : $data['productName'] ?></span>
                    (<span><?= date('Y', $data['date']) ?></span>)
                </div>
                <div>
                    <?= (isset($data['authorName']) ? $data['authorName'] : $data['name']) . " " . strtoupper($data['surname']) ?>
                </div>
            </div>
            <?php
            if (!empty($data['description'])) {
                ?>
                <div><?= LongBookCard::truncate($data['description'], 200) ?></div>
                <?php
            }
            ?>
            <?php
            if (isset($data['price'])) {
                ?>
                <div><span><?= explode(".", $data['price'])[0] ?></span>
                    .<?= explode(".", $data['price'])[1] ?>€
                </div>
                <?php
            }
            if (isset($data['toBeRated'])) {
                ?>
                <div>
                    <form action="index.php?rating" method="post">
                        <div>
                            <div><textarea name="commentValue" placeholder="Entrez un commentaire"></textarea>
                                <input type="hidden" name="bookID" value="<?= $data['bookID'] ?>">
                            </div>
                            <div><label>Note : <input type="range" min="0" max="5" step="1" name="ratingValue"></label>
                            </div>
                            <div>
                                <button type="submit">Noter</button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
            }
            ?>
        </div>
        <?= !isset($data['toBeRated']) ? "</a>" : "" ?>
        <?php
    }
}