<?php
require_once "views/components/Component.php";

/**
 * Composant snackbar
 */

/**
 * Class SnackBar représente une snackbar permettant de transmettre des informations importantes
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class SnackBar implements Component
{
    /**
     * Méthode de génération du composant
     * @param mixed $data les données à afficher
     * @return mixed|void ne renvoie rien
     */
    public static function generate($data)
    {
        ?>
        <div id="snackbar" class="<?= $data['type'] ?>"><?= $data['data'] ?></div>
        <?php
    }
}