<?php
require_once "views/components/Component.php";

/**
 * Composant commentcard
 */

/**
 * Class CommentCard représente un composant de carte de commentaire sur la page d'un produit
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class CommentCard implements Component
{
    /**
     * Méthode de génération du composant
     * @param mixed $data les données à afficher
     * @return mixed|void ne renvoie rien
     */
    public static function generate($data)
    {
        ?>
        <div class="commentCard">
            <div>
                <div>Avis posté par <?= $data['username'] ?></div>
                <div>
                    «<?= $data['comment'] ?>»
                </div>
            </div>
            <div>
                <?php
                for ($i = 0; $i < $data['value']; $i++) {
                    echo '<i class="material-icons">star</i>';
                }
                for ($i = 0; $i < 5 - $data['value']; $i++) {
                    echo '<i class="material-icons">star_border</i>';
                }
                ?>
            </div>
        </div>
        <?php
    }
}