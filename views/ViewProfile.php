<?php
require_once 'views/View.php';

/**
 * Classe viewprofile
 */

/**
 * Class ViewProfile représente la vue d'affichage du profil
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ViewProfile implements View
{
    /**
     * Méthode utilisée pour afficher a page de profil
     * @param mixed $data les données à afficher
     * @return mixed|void
     */
    public function render($data)
    {
        ?>
        <div id="containerProfiles">
            <div class="profileTab">
                <h1>Profil de <?= $data['profile']['username'] ?></h1>
                <div>
                    <h3>Prénom : </h3>
                    <span><?= $data['profile']['name'] ?></span>
                </div>
                <div>
                    <h3>Nom : </h3>
                    <span><?= $data['profile']['surname'] ?></span>
                </div>
                <div>
                    <h3>Adresse : </h3>
                    <span><?= $data['profile']['address'] ?></span>
                </div>
                <div>
                    <h3>Code postal : </h3>
                    <span><?= $data['profile']['postal_code'] ?></span>
                </div>
                <div>
                    <h3>Ville : </h3>
                    <span><?= $data['profile']['city'] ?></span>
                </div>
                <div>
                    <h3>Date de naissance : </h3>
                    <span><?= date('d/m/Y', $data['profile']['birth']) ?></span>
                </div>
            </div>
            <div class="profileTab">
                <h2>Commandes passées : </h2>
                <?php
                if (count($data['commands']) <= 0) {
                    ?>
                    <div>Aucune commande passée</div>
                    <?php
                } else {
                    echo '<div id="containerCommands">';
                    foreach ($data['commands'] as $command) {
                        ?>
                        <div class="commandRow">
                            <div><img src="images/covers/<?= $command['image'] ?>"></div>
                            <div><h3><?= $command['name'] ?></h3></div>
                            <div><h4><?= $command['quantity'] > 1 ? $command['quantity'] . " exemplaires" :
                                        $command['quantity'] . " exemplaire" ?></h4></div>
                            <div>Commandé le <?= date('d/m/Y', $command['commandDate']) ?></div>
                            <div><?= $command['quantity'] * $command['price'] ?>€</div>
                        </div>
                        <?php
                    }
                    echo '</div>';
                }
                ?>
            </div>
            <div class="profileTab">
                <h2>Produits à noter : </h2>
                <div>
                    <?php
                    if (count($data['ratings']) <= 0) {
                        echo '<div>Aucun produit à noter</div>';
                    } else {
                        foreach ($data['ratings'] as $rating) {
                            unset($rating['price']);
                            $rating['toBeRated'] = true;
                            LongBookCard::generate($rating);
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
        if (isset($data['success'])) {
            SnackBar::generate(array("type" => "success", "data" => $data['success']));
        } else if (isset($data['error'])) {
            SnackBar::generate(array("type" => "error", "data" => $data['error']));
        }
    }
}