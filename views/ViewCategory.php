<?php
require_once "views/View.php";
require_once "views/components/LongBookCard.php";

/**
 * Classe viewcategory
 */

/**
 * Class ViewCategory représente la vue d'une catégorie, ou de toutes les catégories
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ViewCategory implements View
{
    /**
     * Méthode utilisée pour afficher les catégories, ou une catégorie particulière
     * @param mixed $data les données à afficher
     * @return mixed|void
     */
    public function render($data)
    {
        if (!is_array($data)) {
            ?>
            <div id="noProducts">
                <h2><?= $data ?></h2>
            </div>
            <?php
        } else {
            if (isset($data['books'])) {
                ?>
                <style>
                    .fab-test {
                        width: 70px;
                        height: 70px;
                        transition: all 0.1s ease-in-out;
                        text-align: center;
                        line-height: 70px;
                        position: fixed;
                        right: 0.5vw;
                        bottom: 6vh;
                        cursor: pointer;
                        z-index: 20;
                        animation: 0.5s popin;
                        display: flex;
                        flex-direction: column;
                        align-items: center;
                        justify-content: center;
                        -webkit-touch-callout: none;
                        -webkit-user-select: none;
                        -moz-user-select: none;
                        -ms-user-select: none;
                        user-select: none;
                    }

                    .fab-test:hover .fab-buttons {
                        opacity: 1;
                        visibility: visible;
                    }

                    .fab-test:hover .fab-buttons__link {
                        transform: scaleY(1) scaleX(1) translateY(-16px) translateX(0px);
                    }

                    .fab-action-button:hover + .fab-buttons .fab-buttons__link:before {
                        visibility: visible;
                        opacity: 1;
                        transform: scale(1);
                        transform-origin: right center 0;
                        transition-delay: 0.3s;
                    }

                    .fab-action-button > i {
                        color: var(--accent-color);
                    }

                    .fab-action-button {
                        position: absolute;
                        bottom: 0;
                        display: block;
                        width: 56px;
                        height: 56px;
                        border-radius: 50%;
                        background-color: var(--secondary-color);
                        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
                    }

                    .fab-buttons {
                        position: absolute;
                        left: 0;
                        right: 0;
                        bottom: 50px;
                        list-style: none;
                        margin: 0;
                        padding: 0;
                        opacity: 0;
                        visibility: hidden;
                        transition: 0.2s;
                        background: transparent;
                    }

                    .fab-buttons__item {
                        display: block;
                        text-align: center;
                        margin: 12px 0;
                    }

                    .fab-buttons__link {
                        display: inline-block;
                        width: 40px;
                        height: 40px;
                        text-decoration: none;
                        border-radius: 50%;
                        transform: scaleY(0.5) scaleX(0.5) translateY(0px) translateX(0px);
                        -moz-transition: .3s;
                        -webkit-transition: .3s;
                        -o-transition: .3s;
                        transition: .3s;
                    }

                    [data-tooltip]:before {
                        top: 50%;
                        margin-top: -11px;
                        font-weight: 600;
                        border-radius: 2px;
                        content: attr(data-tooltip);
                        font-size: 12px;
                        text-decoration: none;
                        visibility: hidden;
                        opacity: 0;
                        padding: 4px 7px;
                        margin-right: 12px;
                        position: absolute;
                        transform: scale(0);
                        right: 100%;
                        white-space: nowrap;
                        transform-origin: top right;
                        transition: all .3s cubic-bezier(.25, .8, .25, 1);
                    }

                    [data-tooltip]:hover:before {
                        visibility: visible;
                        opacity: 1;
                        transform: scale(1);
                        transform-origin: right center 0;
                    }
                </style>
                <div class="fab-test">
                  <span class="fab-action-button">
                        <i class="material-icons">
                        sort
                        </i>
                    </span>
                    <ul class="fab-buttons">
                        <li class="fab-buttons__item">
                            <a href="#" class="fab-buttons__link" data-tooltip="Title">
                                <i class="material-icons">
                                    sort_by_alpha
                                </i>
                            </a>
                        </li>
                        <li class="fab-buttons__item">
                            <a href="#" class="fab-buttons__link" data-tooltip="Date">
                                <i class="material-icons">
                                    date_range
                                </i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div id="productsInCategory">
                    <h1>Produits disponibles</h1>
                    <?php
                    $i = 0;
                    foreach ($data['books'] as $book) {
                        $book['delay'] = $i;
                        LongBookCard::generate($book);
                        $i += 0.1;
                    }
                    ?>
                </div>
                <?php
            } else {
                ?>
                <h2>Catégories disponibles</h2>
                <div id="categories">
                    <?php
                    foreach ($data['categories'] as $category) {
                        CategoryCard::generate($category);
                    }
                    ?>
                </div>
                <?php
            }
            ?>
            <script src="js/category.js" type="text/javascript"></script>
            <?php
        }
    }
}