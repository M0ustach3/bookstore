<?php
require_once "views/View.php";

/**
 * Classe viewcheckout
 */

/**
 * Class ViewCheckout représente la vue du paiement
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ViewCheckout implements View
{
    /**
     * Méthode utilisée pour afficher la page de paiement
     * @param mixed $data les données à afficher
     * @return mixed|void
     */
    public function render($data)
    {
        $userInfo = $data[1];
        $total = 0;
        ?>
        <form action="index.php?confirmCheckout" method="post">
            <div id="checkout">
                <div>
                    <div class="checkoutCategoryHeader">
                        <div>
                            <div>1</div>
                            <div>Récapitulatif de la commande</div>
                        </div>
                        <div><i class="material-icons">
                                unfold_more
                            </i></div>
                    </div>
                    <div>
                        <?php
                        foreach ($data[0] as $book) {
                            $book['book']['description'] = $book['book']['price'] . "€ l'unité";
                            $book['book']['price'] *= $book['quantity'];
                            $book['book']['productName'] .= ($book['quantity'] > 1 ? " (" . $book['quantity'] . " exemplaires)" : " (" . $book['quantity'] . " exemplaire)");
                            LongBookCard::generate($book['book']);
                            $total += $book['book']['price'];
                        }
                        ?>
                    </div>
                </div>
                <div>
                    <div class="checkoutCategoryHeader">
                        <div>
                            <div>2</div>
                            <div>Adresse de livraison</div>
                        </div>
                        <div><i class="material-icons">
                                unfold_more
                            </i></div>
                    </div>
                    <div>
                        <div>
                            <div><input type="text" name="address" placeholder="Adresse"
                                        <?= (isset($userInfo['address']) && !empty($userInfo['address'])) ? "value=\"" . $userInfo['address'] . "\"" : "" ?>>
                            </div>
                        </div>
                        <div>
                            <div><input type="text" name="postalCode" placeholder="Code Postal"
                                        <?= (isset($userInfo['postal_code']) && !empty($userInfo['postal_code'])) ? "value=\"" . $userInfo['postal_code'] . "\"" : "" ?>>
                            </div>
                            <div><input type="text" name="city" placeholder="Ville"
                                        <?= (isset($userInfo['city']) && !empty($userInfo['city'])) ? "value=\"" . $userInfo['city'] . "\"" : "" ?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="checkoutCategoryHeader">
                        <div>
                            <div>3</div>
                            <div>Mode de paiement</div>
                        </div>
                        <div><i class="material-icons">
                                unfold_more
                            </i></div>
                    </div>
                    <div>
                        <div><label>
                                Visa
                                <input type="radio" name="payment" value="cb">
                            </label>
                            <div>
                                <input type="text" name="visaNumber" placeholder="Numéro de carte">
                                <input type="text" name="visaHolder" placeholder="Propriétaire">
                                <input type="text" name="visaCode" placeholder="CVV">
                            </div>
                        </div>
                        <div>
                            <label>
                                MasterCard
                                <input type="radio" name="payment" value="mastercard">
                            </label>
                            <div>
                                <input type="text" name="mastercardNumber" placeholder="Numéro de carte">
                                <input type="text" name="mastercardHolder" placeholder="Propriétaire">
                                <input type="text" name="mastercardCode" placeholder="CVV">
                            </div>
                        </div>
                        <div>
                            <label>
                                Paypal
                                <input type="radio" name="payment" value="paypal">
                            </label>
                            <input type="text" name="paypalAccount" placeholder="Compte PayPal">
                        </div>
                        <div>
                            <label>
                                Bitcoin
                                <input type="radio" name="payment" value="bitcoin">
                            </label>
                            <input type="text" name="bitcoinAddress" placeholder="Adresse Bitcoin">
                        </div>
                    </div>
                </div>
                <div>
                    <button type="submit">Valider le paiement (<?= $total ?>€)</button>
                </div>
            </div>
        </form>
        <script src="js/checkout.js" type="text/javascript"></script>
        <?php
    }
}