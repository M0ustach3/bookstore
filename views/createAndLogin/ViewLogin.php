<?php
require_once 'views/View.php';
require_once "views/components/SnackBar.php";

/**
 * Classe login
 */

/**
 * Class ViewLogin représente la vue de connexion
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ViewLogin implements View
{
    /**
     * Méthode utilisée pour afficher la page de connexion
     * @param mixed $data les données à afficher
     * @return mixed|void
     */
    public function render($data)
    {
        ?>
        <div class="login-page">
            <h1>Connexion</h1>
            <div class="form fadeInBottom">
                <form class="login-form" action="index.php?connect"
                      method="post">
                    <input type="text" placeholder="Utilisateur" name="user" autofocus autocomplete="off" required/>
                    <input type="password" placeholder="Mot de passe" name="password" autocomplete="off" required/>
                    <button type="submit" name="se connecter">
                        se connecter
                    </button>
                    <p class="message"><a href="index.php?connect&new">Créer un compte</a></p>
                </form>
            </div>
        </div>
        <script src="js/login.js" type="text/javascript"></script>
        <?php
        if (!empty($data)) {
            if (!empty($data['error'])) {
                SnackBar::generate(array('type' => 'error', 'data' => $data['error']));
            } else if (!empty($data['success'])) {
                SnackBar::generate(array('type' => 'success', 'data' => $data['success']));
            }
        }
    }
}