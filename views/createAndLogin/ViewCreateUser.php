<?php
require_once 'views/View.php';
require_once "views/components/SnackBar.php";

/**
 * Classe createuser
 */

/**
 * Class ViewCreateUser représente la vue de création d'un utilisateur
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ViewCreateUser implements View
{
    /**
     * Méthode utilisée pour afficher la page de création de compte
     * @param mixed $data les données à afficher
     * @return mixed|void
     */
    public function render($data)
    {
        ?>
        <div class="login-page">
            <h1>S'inscrire</h1>
            <div class="form">
                <form class="login-form" action="index.php?connect&new"
                      method="post" name="createUserForm">
                    <input type="text" placeholder="Nom" name="surname" autocomplete="off"
                           value="<?= isset($data['values']) ? $data['values']['surname'] : '' ?>"/>
                    <input type="text" placeholder="Prénom" name="name" autocomplete="off"
                           value="<?= isset($data['values']) ? $data['values']['name'] : '' ?>"/>
                    <input type="text" placeholder="Adresse" name="address" autocomplete="off"
                           value="<?= isset($data['values']) ? $data['values']['address'] : '' ?>"/>
                    <input type="text" placeholder="Code postal" name="zipcode" autocomplete="off"
                           value="<?= isset($data['values']) ? $data['values']['zipcode'] : '' ?>"/>
                    <input type="text" placeholder="Ville" name="city" autocomplete="off"
                           value="<?= isset($data['values']) ? $data['values']['city'] : '' ?>"/>
                    <input type="date" placeholder="Date de naissance" name="birth" autocomplete="off"
                           value="<?= isset($data['values']) ? $data['values']['birth'] : '' ?>"/>
                    <input type="text" placeholder="Utilisateur" name="user" autocomplete="off"
                           value="<?= isset($data['values']) ? $data['values']['user'] : '' ?>" required/>
                    <input type="password" placeholder="Mot de passe" name="password" autocomplete="off" required/>
                    <button type="submit" name="se connecter">
                        créer mon compte
                    </button>
                </form>
            </div>
        </div>
        <script src="js/login.js" type="text/javascript"></script>
        <?php
        if (!empty($data)) {
            if (!empty($data['error'])) {
                SnackBar::generate(array('type' => 'error', 'data' => $data['error']));
            } else if (!empty($data['success'])) {
                SnackBar::generate(array('type' => 'success', 'data' => $data['success']));
            }
        }
    }
}