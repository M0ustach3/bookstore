<?php
/**
 * Interface de view
 */

/**
 * Interface View représentant une vue
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
interface View
{
    /**
     * Méthode utilisée pour afficher une vue
     * @param $data mixed d'éventuelles données à afficher
     * @return mixed ne renvoie rien dans tous les cas utilisés dans le projet
     */
    public function render($data);
}