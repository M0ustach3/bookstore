<?php
require_once "views/components/BookCard.php";
require_once "views/components/CategoryCard.php";
require_once 'views/View.php';
/**
 * Class viewhome
 */

/**
 * Class ViewHome représente la vue de page d'accueil
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ViewHome implements View
{
    /**
     * Méthode utilisée pour afficher la page d'accueil
     * @param mixed $data les données à afficher
     * @return mixed|void
     */
    public function render($data)
    {
        ?>
        <div class="containerHome">
            <h2>Livres aléatoires</h2>
            <section>
                <?php
                $i = 0;
                foreach ($data[0] as $livre) {
                    BookCard::generate(array('data' => $livre, 'delay' => $i));
                    $i += 0.2;
                }
                ?>
            </section>
            <h2>Catégories</h2>
            <section>
                <?php
                foreach ($data[1] as $category) {
                    CategoryCard::generate($category);
                }
                ?>
            </section>
        </div>

        <?php
    }
}