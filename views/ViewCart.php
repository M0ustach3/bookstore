<?php
require_once "views/View.php";
require_once "views/components/LongBookCard.php";

/**
 * Classe viewcart
 */

/**
 * Class ViewCart représente la vue d'affichage de panier
 * @author Pablo Bondia-Luttiau et Lennaïg Charron
 * @copyright 2020, ESIEA
 * @license https://opensource.org/licenses/GPL-3.0 GNU GPLv3
 */
class ViewCart implements View
{
    /**
     * Méthode utilisée pour afficher le panier
     * @param mixed $data les données à afficher
     * @return mixed|void
     */
    public function render($data)
    {
        ?>
        <div id="cart">
            <h1>Panier</h1>
            <?php
            if (!empty($data)) {
                $total = 0;
                foreach ($data as $item) {
                    LongBookCard::generate($item['book']);
                    ?>
                    <h4>Sous total des <?= $item['quantity'] ?> <span><?= $item['book']['productName'] ?></span>
                        : <?= $item['book']['price'] * $item['quantity'] ?>€</h4>
                    <?php
                    $total += $item['book']['price'] * $item['quantity'];
                }
                ?>
                <div>
                </div>
                <span>

            </span>
                <div id="total">
                    <div>
                        <h2>Total : </h2>
                        <h2><?= $total ?>€</h2>
                    </div>
                    <div>
                        <a href="index.php?checkout">Acheter</a>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <h2>Aucun article dans le panier :(</h2>
                <?php
            }
            ?>
        </div>
        <?php
    }
}